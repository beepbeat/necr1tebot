package com.beepbeat.twitchBot;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by Zacharias on 06.04.2015.
 */

public class ServletContextProcessor {

    @PostConstruct
    void onStartup() {
        new com.beepbeat.twitchBot.MyBot();
    }

    @PreDestroy
    void atShutdown(){

    }

}
