package com.beepbeat.twitchBot;


import com.beepbeat.twitchBot.Config;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;
import org.apache.commons.io.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MyBot extends PircBot {
    private static String channelname = "#necr1te";
    //private static MyBot bot = Main.getBot();
    private static List<String> adderList;// = new ArrayList<String>();
    //private static String name = "Necr1teBot";
    private List<String> oldHosts = new ArrayList<String>();
    List<String> modList = new ArrayList<String>();
    boolean greetOnJoin = false;
    List<String> oldFollower = new ArrayList<String>();
    boolean nightbot = false;
    List<String> allowedTwitchLinks;
    static String caller = "";
    String oldCaller = "";
    int loses = 0;
    int wins = 0;

    public MyBot(){
        this.setName("Necr1teBot");
        try {
            this.connect("irc.twitch.tv",6667,"oauth:rj4gqmro38vo65wwdqxncyu7lmgync");
            this.joinChannel(channelname);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IrcException e) {
            e.printStackTrace();
        }
        //sendMessage(channelname,"Hallo, Ich bin Necr1teBot der Geschichtensammler. Mein Erschaffer ist Holladiewal");
        //sendMessage(channelname, "Necr1teBot ist jetzt online!");
        //sendMessage(channelname,"$ping");
        sendMessage(channelname,Config.randomQuote());
        reloadPerm();
        getHosts();
        //System.out.println(getFollowerList());
        oldFollower = Config.loadFollows();
        //getFollowerList();


    }
    @Override
    public void onMessage(String channel, String sender, String login, String hostname, String message){
        if (sender.equalsIgnoreCase(this.getNick()) || sender.equalsIgnoreCase(this.getName())){
            System.out.println("ignoring message from my self");
            //this.sendMessage(channelname,"Ich ingnoriere alle Befehle die von mir gesendet wurden!!");
            //this.sendMessage(channelname,"Hey, Hey, Hey. Ich nicht dich lassen machen mich selbst befehligen!");
            return;
        }
        if (message.contains("$")){
            if (message.equalsIgnoreCase("$ping")){
                sleep((long) 0.5);
                System.out.println("recieved user ping");
               //bot.joinChannel(channelname);
                this.sendMessage(channelname, "Ping");
                //bot.sendMessage(channelname,"Ping2");

            }
            if (message.contains("$quote")){
                List<String> params = Arrays.asList(message.split(" "));
                //params.remove(0);
                if (params.get(1).equalsIgnoreCase("add")) {
                    //this.sendMessage(channelname, "Zitate temporär gesperrt");
                    if (adderList.contains(sender)) {

                        Calendar calendar = Calendar.getInstance();
                        String quoteText = "";
                        for (int i = 3; i < params.size(); i++) {
                            String s = params.get(i);
                            quoteText = quoteText + s + " ";
                        }

                        String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
                        String Quote = quoteText + " - " + params.get(2).toLowerCase() + ", " + timeStamp;
                        System.out.println(Quote);
                        this.sendMessage(channelname, sender + ": Zitat hinzugefügt");
                        Config.addQuote(Quote);
                    }
                    else{
                        this.sendMessage(channelname,"Tut mir Leid, du darfst das nicht");
                    }
                }else {
                    //this.sendMessage(channelname,"Zitate temporär gesperrt");
                    this.sendMessage(channelname,Config.randomQuote(params.get(1).toLowerCase()));
                }

            }
            if (message.equalsIgnoreCase("$debug")){
                sendMessage(channelname,channel+","+sender+","+login);
            }
            if (message.equalsIgnoreCase("$reloadperm")){reloadPerm();this.sendMessage(channelname,"Berechtigungen neugeladen");}
            if (message.equalsIgnoreCase("$help")){
                this.sendMessage(channelname,"Ich zitiere, mehr nicht. Naja ping kann ich auch noch sagen.");
                sleep((long) 2.5);
                this.sendMessage(channelname,"Zitieren kannst du mit :\"$quote name\"");
                sleep((long) 2);
                this.sendMessage(channelname,"Zitate fügst du mit \"$quote add name Zitat\" hinzu");
                sleep((long) 2);
                this.sendMessage(channelname, "Ping mach ich auf \"$ping\"");

            }
            if (message.contains("$addperms")) {
                if (sender.equalsIgnoreCase("holladiewal") || sender.equalsIgnoreCase("necr1te") || modList.contains(sender)) {//
                    List<String> params = Arrays.asList(message.split(" "));
                    if (params.get(1).equalsIgnoreCase("add")) {
                        Config.addAddingPerms(params.get(2));
                    }
                    if (params.get(1).equalsIgnoreCase("remove")){
                        Config.remAddingPerms(params.get(2));
                    }
                }
                else{
                    this.sendMessage(channelname,"Tut mir Leid, du darfst das nicht!");
                }
            }
            if (message.contains("$greet")){
                if (sender.equalsIgnoreCase("holladiewal") || modList.contains(sender)) {
                    List<String> params = Arrays.asList(message.split(" "));
                    if (params.get(1).equalsIgnoreCase("true")) {
                        greetOnJoin = true;
                    }
                    if (params.get(1).equalsIgnoreCase("false")) {
                        greetOnJoin = false;
                    }

                }
            }
            if (message.equalsIgnoreCase("$modlist")){
                this.sendMessage(channelname,modList.toString());
            }
            if (message.equalsIgnoreCase("$mcinfo")){
                this.sendMessage(channelname,"Gespielt wird Minecraft 1.8 für den PC auf dem öffentlichem Server Minersheaven (IP: 144.76.110.183) mit dem Texture Pack \"Chroma Hills 128x\". Noch Fragen?");
            }
            if (message.equalsIgnoreCase("$timer")){
                List<String> params = Arrays.asList(message.split(" "));
                String endTime = params.get(1);
            }
            if (message.equalsIgnoreCase("$dududu")){
                if (sender.equalsIgnoreCase("holladiewal") || modList.contains(sender)){
                    this.sendMessage(channelname,"dudududududuududududududududuudududuududududuududududududuududu");
                }
            }
            if (message.equalsIgnoreCase("$musikb")){
                if (sender.equalsIgnoreCase("holladiewal") || modList.contains(sender)){
                    this.sendMessage(channelname,"Musikwunschrunde, Bastiiiiiiiiiiiiiiiiiiiii");
                }
            }
            if (message.equalsIgnoreCase("$musik")){
                this.sendMessage(channelname,"Es geht los mit den Musikwünschen. Bitte im Format \"@Necr1te: Interpret - Titel \"  ");
            }
            if (message.equalsIgnoreCase("$musikende")){
                this.sendMessage(channelname,"Und vorbei ist es mit der Musikwunschrunde! Bitte keine weiteren Wünsche!!");
            }
            if (message.equalsIgnoreCase("$bastiii")) {
                if (sender.equalsIgnoreCase("holladiewal") || modList.contains(sender)) {
                    this.sendMessage(channelname, "Bastiiiiiiiiiiiiiii, ich soll dich an was erinnern!!!!");

                }
            }
            if (message.equalsIgnoreCase("$20")){
                this.sendMessage(channelname,"Also Bastiiii, willste wieder später starten?!?!??!?!??!???!??!?!?");
            }
            if (message.equalsIgnoreCase("$battletag")){
                this.sendMessage(channelname,"Necr1tes BattleTag ist: Necr1te#2875");
            }
            if (message.equalsIgnoreCase("$sm")){
                this.sendMessage(channelname,"Einmal teilen, liken und Co.: http://www.facebook.com/necr1te und http://www.twitter.com/necr1teview ! Oder auch zweimal ;) oder Dreimal, ihr kennt das Spielchen");
            }
            if (message.contains("$score")) {
                System.out.println("score request recieved");
                List<String> params = null;
                if (message.contains(" ")) {
                    params = Arrays.asList(message.split(" "));
                }
                if (params == null) {
                    this.sendMessage(channelname, "Necr1te hat " + String.valueOf(loses) + " mal verloren und " + String.valueOf(wins) + " mal gewonnen");
                }
                else {
                    if (params.get(1).equalsIgnoreCase("lose")) {
                        loses++;
                        this.sendMessage(channelname, "Niederlage eingetragen");
                        System.out.println("verloren!");
                    }
                    if (params.get(1).equalsIgnoreCase("win")) {
                        wins++;
                        this.sendMessage(channelname, "Sieg eingetragen");
                        System.out.println("gewonnen!");
                    }
                }
            }
            //Necr1te#2875
            if (message.equalsIgnoreCase("$sendeplan")){
                String date = new SimpleDateFormat("EE").format(Calendar.getInstance().getTime());
                //System.out.println(date);
                if (date.equalsIgnoreCase("mo")){}
                if (date.equalsIgnoreCase("di")){
                    this.sendMessage(channelname,"Offline");
                }
                if (date.equalsIgnoreCase("mi")){
                    this.sendMessage(channelname,"Vllt. live um 14.00. Auf jeden Fall live um 20.00");
                }
                if (date.equalsIgnoreCase("do")){
                    this.sendMessage(channelname,"Live um 20.00!");
                }
                if (date.equalsIgnoreCase("fr")){
                    this.sendMessage(channelname,"Live um 20.00 mit Heroes of the Storm!");
                }
                if (date.equalsIgnoreCase("sa")){}
                if (date.equalsIgnoreCase("so")){}

            }

        }
        if (message.contains("http://") || message.contains("https://") && (!sender.equalsIgnoreCase("necr1te") || modList.contains(sender)) ){//
            if (message.contains("www.strawpoll.me")){
                if (nightbot) {
                    this.sendMessage(channelname, sender + ": Auch wenn wir Umfragen gerne machen, wenn diese zum Thema passen, darfst du keine Links posten. Diesen dürfte der NightBot jetzt auch schon gefresssen haben.");
                }
                else{
                    this.sendMessage(channelname, sender + ": Auch wenn wir Umfragen gerne machen, wenn diese zum Thema passen, darfst du keine Links posten.");
                }
            }


            if (message.contains("www.twitch.tv") && !allowedTwitchLinks.contains(getTwitchLink(message))){
                if (nightbot) {
                    this.sendMessage(channelname, sender + ": Bitte keine Werbung für andere Streamer!!");
                }
                else{
                    this.sendMessage(channelname, sender + ": Bitte keine Werbung für andere Streamer!");
                }
            }
            else{
                //this.sendMessage(channelname,sender + ": Bitte keine Links!");
            }
            if (!nightbot){
                this.sendMessage(channelname,"Mmmmm, der Link war lecker!!");
                this.sendMessage(channelname,".timeout " + sender + " 1");
            }
        }
    //getFollowerList();
    printNewHost();
        if (!caller.equalsIgnoreCase(oldCaller)){
            this.sendMessage(channelname,caller);
            oldCaller = caller;
        }
    }

    @Override
    protected void onPing(String sourceNick, String sourceLogin, String sourceHostname, String target, String pingValue){
        sendRawLine("PONG " + pingValue);

    }
    @Override
    protected void onOp(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient){
        modList.add(recipient);
        System.out.println("opped: " + recipient);
    }
    @Override
    protected void onDeop(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient){
        modList.remove(recipient);
    }
    @Override
    protected void onUserMode(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
        List<String> params = Arrays.asList(mode.split(" "));
        if (params.get(1).equalsIgnoreCase("+o")){
            modList.add(params.get(2));
        }
        if (params.get(1).equalsIgnoreCase("-o")){
            modList.remove(params.get(2));
        }
        System.out.println("recieved mode set:" + mode + ", " + targetNick);
    }

    @Override
    protected void onJoin(String channel, String sender, String login, String hostname){
        if (sender.equalsIgnoreCase("nightbot")){nightbot = true;}
        else {
            //noinspection StatementWithEmptyBody
            if(sender.equalsIgnoreCase("moobot")){}
            else{
                if (greetOnJoin) {
                    String quote = Config.randomQuote(sender);
                    if (!quote.equalsIgnoreCase("Kein Zitat gefunden!")){
                        this.sendMessage(channelname,quote);
                        System.out.println("greeted: " + sender + " with " + quote);
                    }
                    else{
                        this.sendMessage(channelname, "huhu " + sender);

                    }
                }

            }
        }
    }

    @Override
    protected void onPart(String channel, String sender, String login, String hostname){
        if (sender.equalsIgnoreCase("nightbot")){
            nightbot = false;
        }
    }


    public void sleep(long time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void reloadPerm(){
        /*adderList.clear();
        adderList.add("necr1te");
        adderList.add("holladiewal");
        adderList.add("mabulon");
        adderList.add("lyndariel");
        adderList.add("blackburn2014");
        //adderList.add("");*/
        adderList = Config.getAddingPerms();
        System.out.println("reloadedPermissions");
    }
    public static List<String> getHosts(){

        URL url = null;
        try {
            url = new URL("http://chatdepot.twitch.tv/rooms/" + channelname.replace("#","") +"/hosts");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        assert url != null;
        URLConnection con = null;
        try {
            con = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream in = null;
        try {
            assert con != null;
            in = con.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String encoding = con.getContentEncoding();
        encoding = encoding == null ? "UTF-8" : encoding;
        String body = null;
        try {
            assert in != null;
            body = IOUtils.toString(in, encoding);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(body);

        assert body != null;
        body = body.replace("{\"hosts\":[","").replace("]}","");
        if (body.contains("{\"host\":")){body = body.replace("{\"host\":","").replace("}", "");}
        //                 {"host":
        if (body.contains(",")) {
            return Arrays.asList(body.split(","));
        }
        else{
            return Arrays.asList(body);
        }
        //return null;
    }
    public void printNewHost(){
        List<String> hosts = getHosts();
        List<String> newHosts = new ArrayList<String>();
        for (String s : hosts) {
            if (!oldHosts.contains(s)){
                newHosts.add(s);
            }
        }
        if (newHosts.size() > 0){
            String newHostsStr = "";
            for (String s : newHosts){
                newHostsStr = newHostsStr  + s + " ";
            }
            this.sendMessage(channelname, "Neue Hosts: " + newHostsStr);
        }
    oldHosts = hosts;
    }

    public List<String> getModList(){
        return modList;
    }

    public void getFollowerList() {
        List<String> followerNames = new ArrayList<String>();
        URL followerList = null;
        try {
            followerList = new URL("https://api.twitch.tv/kraken/channels/necr1te/follows");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        assert followerList != null;
        URLConnection con = null;
        try {
            con = followerList.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream in = null;
        try {
            assert con != null;
            in = con.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String encoding = con.getContentEncoding();
        encoding = encoding == null ? "UTF-8" : encoding;
        String body = null;
        try {
            assert in != null;
            body = IOUtils.toString(in, encoding);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        JSONParser parser = new JSONParser();
//        JSONArray all = null;
//        try {
//            System.out.print(parser.parse(body));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        assert body != null;

        JSONObject all = (JSONObject) JSONValue.parse(body);
        JSONArray follows = (JSONArray) all.get("follows");


        for (int i = 0; i < 25;i++){
            JSONObject follower = (JSONObject) follows.get(i);
            JSONObject user = (JSONObject) follower.get("user");
            String displayname = (String) user.get("display_name");
            followerNames.add(displayname);
        }

        List<String> follower = followerNames;
        List<String> newFollower = new ArrayList<String>();
        for (String s : follower){
            if (!oldFollower.contains(s)){
                //newFollower.add(s);
                this.sendMessage(channelname,"Danke fürs follownieren, " + s + " !!!!!!!!");
                System.out.println(s + " follownierte");
            }
        }

        oldFollower = follower;
        Config.saveFollows(oldFollower);

        //return followerNames;
    }

    /*public void getNewFollower(){
        List<String> follower = getFollowerList();
        List<String> newFollower = new ArrayList<String>();
        for (String s : follower){
            if (!oldFollower.contains(s)){
                //newFollower.add(s);
                this.sendMessage(channelname,"Danke fürs follownieren, " + s + " !!!!!!!!");
                System.out.println(s + " follownierte");
            }
        }

        oldFollower = follower;
        Config.saveFollows(oldFollower);
    }*/

    public String getTwitchLink(String input){
        String[] splitResult = null;
        splitResult = input.split("www.");
        splitResult = splitResult[1].split("/");
        String name = splitResult[1];
        return "www.twitch.tv/" + name;
    }

    public static void send(String msg){
        caller = msg;
    }



}
