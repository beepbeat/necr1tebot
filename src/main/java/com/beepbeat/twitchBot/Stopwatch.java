package com.beepbeat.twitchBot;


public class Stopwatch {
    long startTime;
    long stopTime;


    public Stopwatch(){

    }

    public void start(){
        startTime = System.nanoTime();
        //System.currentTimeMillis();
    }
    public void stop(){
        stopTime = System.nanoTime();
    }
    public long nanoRuntime(){
        return stopTime - startTime;
    }
}
