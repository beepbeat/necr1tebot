package com.beepbeat.twitchBot;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class Config {

    static File quotesFile = new File("quotes.cfg");
    static File adderFile = new File("adder.cfg");
    static File followerFile = new File("follower.cfg");
    static File commandsFile = new File("commands.cfg");

    public static List<String> readQuotes() {
        if (!quotesFile.exists()){
            try {
                Files.createFile(quotesFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<String> returnV = null;
        try {
            returnV = Files.readAllLines(quotesFile.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnV;
    }

    public static String randomQuote(){
        List<String> quotes = readQuotes();
        int random = (int) MathUtil.Random(quotes.size() - 1,true);
        return quotes.get(random);
    }
    public static String randomQuote(String name){

        List<String> quotes = readQuotes();
        List<String> validQuotes = new ArrayList<String>();
        //int random = (int) MathUtil.Random(quotes.size() - 1, true);
        for (String s : quotes){
            if(s.equalsIgnoreCase("")){continue;}
            String s1 = s.split("-")[1];
            //int tmp = s1.split(",").length - 1;
            s1 = s1.split(",")[0];
            if (s1.trim().equalsIgnoreCase(name)){
                validQuotes.add(s);
            }
        }

            if (validQuotes.size() == 0) {
                return "Kein Zitat gefunden!";
            }
            int random = (int) MathUtil.Random(validQuotes.size() - 1, true);

            return validQuotes.get(random);
    }

   /*public static String randomQuote(String name){
       List<String> quotes = readQuotes();
       List<String> validQuotes = new ArrayList<String>();
       for (String s : quotes){
           if (s.split("-")[1].contains(name)){
               validQuotes.add(s);
           }
       }
       int random = (int) MathUtil.Random(validQuotes.size() - 1, true);
       return validQuotes.get(random);


   }*/
    public static void addQuote(String quote){
        List<String> quotes = readQuotes();
        quotes.add(quote);
        try {
            //Files.newBufferedWriter(quotesFile.toPath(), Charset.defaultCharset(), StandardOpenOption.APPEND).write(quote + "\n");
            Files.write(quotesFile.toPath(),quotes,Charset.defaultCharset(),StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static List<String> getAddingPerms(){
        List<String> perms = null;
        try {
            perms = Files.readAllLines(adderFile.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert perms != null;
        return perms;
    }
    public static void addAddingPerms(String name){
        if (!adderFile.exists()){
            try {
                Files.createFile(adderFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<String> permList = getAddingPerms();
        permList.add(name);
        try {
            Files.write(adderFile.toPath(), permList, Charset.defaultCharset(), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void remAddingPerms(String name){
        List<String> permList = getAddingPerms();
        permList.remove(name);
        try {
            Files.write(adderFile.toPath(), permList, Charset.defaultCharset(), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void saveFollows(List<String> list){
        try {
            Files.write(followerFile.toPath(),list,Charset.defaultCharset(),StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static List<String> loadFollows(){
        List<String> rtrV = null;
        try {
            rtrV = Files.readAllLines(followerFile.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert rtrV != null;
        return rtrV;
        
    }
    public static List<String> getCommands(){
        List<String> rtrV = null;
        try {
            rtrV = Files.readAllLines(commandsFile.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert rtrV != null;
        return rtrV;
    }


    public static void addCommand(String cmd, String message){
        List<String> commands = getCommands();
        commands.add(cmd + " - " + message);
        try {
            Files.write(commandsFile.toPath(), commands, Charset.defaultCharset(), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
